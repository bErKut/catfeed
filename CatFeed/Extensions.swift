//
//  Extensions.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 03.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import UIKit

extension Data {
    func decode<T: Decodable>() -> T? {
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(T.self, from: self)
        }
        catch {
            print("Decoding failed with error: \(error)")
            return nil
        }
    }
}

let sectionFooterElementKind = "section-footer-element-kind"

extension UICollectionViewCompositionalLayout {
    private enum Const {
        static let cellHeight: CGFloat = 66
    }
    static var list: UICollectionViewCompositionalLayout {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(1)
        ))
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .absolute(Const.cellHeight)
            ),
            subitems: [item]
        )
        
        let footerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                     heightDimension: .estimated(80))
        let sectionFooter = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: footerSize,
            elementKind: sectionFooterElementKind, alignment: .bottom)

        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [sectionFooter]
        return UICollectionViewCompositionalLayout(section: section)
    }
}

extension Array where Element: NSLayoutConstraint {
    func activate() {
        NSLayoutConstraint.activate(self)
    }
}

extension UIView {
    func pin(toParent parent: UIView,
             withEdgeInsets edgeInsets: UIEdgeInsets = .zero) {
        self.translatesAutoresizingMaskIntoConstraints = false

        [leadingAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.leadingAnchor, constant: edgeInsets.left),
         topAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.topAnchor, constant: edgeInsets.top),
         trailingAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.trailingAnchor, constant: edgeInsets.right),
         bottomAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.bottomAnchor, constant: edgeInsets.bottom)
        ].activate()
    }
    
    func center(inParent parent: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        [centerXAnchor.constraint(equalTo: parent.centerXAnchor),
         centerYAnchor.constraint(equalTo: parent.centerYAnchor)
        ].activate()
    }
}

extension Data {
    func image(asThumbnail: Bool) -> UIImage? {
        if asThumbnail {
            let options = [
                kCGImageSourceCreateThumbnailWithTransform: true,
                kCGImageSourceCreateThumbnailFromImageAlways: true,
                kCGImageSourceThumbnailMaxPixelSize: 100] as CFDictionary

            guard let source = CGImageSourceCreateWithData(self as CFData, nil) else { return nil }
            guard let imageReference = CGImageSourceCreateThumbnailAtIndex(source, 0, options) else { return nil }
            return UIImage(cgImage: imageReference)
        } else {
            return UIImage(data: self)
        }
    }
}
