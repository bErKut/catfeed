//
//  SceneDelegate.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 03.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let window = UIWindow(windowScene: windowScene)
        let store = Store(callbackQueue: DispatchQueue(label: "com.vladberkuta.CatFeed.store.callback.queue"))
        let rootViewController = CatsViewController(store: store)
        window.rootViewController = UINavigationController(rootViewController: rootViewController)
        window.makeKeyAndVisible()
        self.window = window

    }
}

