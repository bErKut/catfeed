//
//  Responses.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 13.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import Foundation

struct Cat: Codable, Hashable, Identifiable {
    let url: String
    let width: Int
    let height: Int
    
    // needed only for pretty-print feature
    private let categories: [Category]?
    private let breeds: [Breed]?
    
    let id: String
    
    private enum CodingKeys: String, CodingKey {
        case url, width, height, categories, breeds, id
    }
    
    private struct Category: Codable, Hashable {
        let id: Int
        let name: String
    }
    
    private struct Breed: Codable, Hashable {
        enum CodingKeys: String, CodingKey {
            case id
            case name
            case temperament
            case lifeSpan = "life_span"
            case altNames = "alt_names"
            case wikipediaURL = "wikipedia_url"
            case origin = "origin"
            case weightImperial = "weight_imperial"
            case experimental = "experimental"
            case hairless = "hairless"
            case natural = "natural"
            case rare
            case rex
            case suppressTail = "suppress_tail"
            case shortLegs = "short_legs"
            case hypoallergenic
            case adaptability
            case affectionLevel = "affection_level"
            case countryCode = "country_code"
            case childFriendly = "child_friendly"
            case dogFriendly = "dog_friendly"
            case energyLevel = "energy_level"
            case grooming
            case healthIssues = "health_issues"
            case intelligence
            case sheddingLevel = "shedding_level"
            case socialNeeds = "social_needs"
            case strangerFriendly = "stranger_friendly"
            case vocalisation
        }
        
        let id: String
        let name: String
        let temperament: String
        let lifeSpan: String
        let altNames: String?
        let wikipediaURL: String?
        let origin: String
        let weightImperial: String?
        let experimental: Int
        let hairless: Int
        let natural: Int
        let rare: Int
        let rex: Int
        let suppressTail: Int?
        let shortLegs: Int
        let hypoallergenic: Int
        let adaptability: Int
        let affectionLevel: Int
        let countryCode: String
        let childFriendly: Int
        let dogFriendly: Int
        let energyLevel: Int
        let grooming: Int
        let healthIssues: Int
        let intelligence: Int
        let sheddingLevel: Int
        let socialNeeds: Int
        let strangerFriendly: Int
        let vocalisation: Int
    }
}
