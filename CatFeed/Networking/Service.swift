//
//  Service.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 03.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import Foundation

enum ServiceError: Error {
    case catsList(Error?)
    case loadImage(Error?)
    case fetchFullImage(Error?)
}

protocol ServiceProtocol {
    func fetchCats(forPage page: Int,
                   limit: Int,
                   completion: @escaping (Result<CatsSearchData, ServiceError>) -> Void)
    func fetchFullImage(withId: String, completion: @escaping (Result<Cat, Error>) -> Void)
    func loadImage(for url: URL, completion: @escaping (Result<Data, Error>) -> Void)
}

struct CatsSearchData {
    let cats: [Cat]
    let availableItemsCount: Int
    let currentPage: Int
}

class Service: ServiceProtocol {
    private enum k {
        static let apiKey = (name: "x-api-key", value: "4ebeb96d-259e-49c4-9886-0ee396ef7634")
        static let baseURL = "api.thecatapi.com"
        static let scheme = "https"
        static let searchRequestPath = "/v1/images/search"
        static let fullImageRequestPath = "/v1/images/"
        static let limit = "limit"
        static let page = "page"
        static let order = (name: "order", value: "desc")
        static let size = (name: "size", value: "thumb")
        static let mimeTypes = (name: "mime_types", value: "jpg,png")
        
        enum PaginationHeaders {
            static let count = "pagination-count"
            static let page = "pagination-page"
        }
        
        static let requestTimeout: Double = 180
    }
    
    private let session: URLSession
    private let baseURL: String
    private var downloadTasks: [String: URLSessionDownloadTask] = [:]
    private let lock = NSLock()
    
    init(baseURL: String = k.baseURL,
         callbackQueue: OperationQueue = OperationQueue.main) {
        assert(!baseURL.isEmpty, "base URL was not provided")
        
        self.baseURL = baseURL
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = k.requestTimeout
        session = URLSession(configuration: configuration, delegate: nil, delegateQueue: callbackQueue)
    }
    
    func fetchCats(forPage page: Int,
                   limit: Int,
                   completion: @escaping (Result<CatsSearchData, ServiceError>) -> Void) {
        assert(page >= 0, "page number could not be negative")
        
        let url = buildSearchImagesURL(page: page, limit: limit)
        let task = buildTask(withURL: url) { data, response, error in
            guard let cats: [Cat] = data?.decode(),
                let httpResponse = response as? HTTPURLResponse,
                let totalItemsCount = Int(httpResponse.allHeaderFields[k.PaginationHeaders.count] as! String),
                let currentPage = Int(httpResponse.allHeaderFields[k.PaginationHeaders.page] as! String) else {
                    completion(.failure(ServiceError.catsList(error)))
                    return
            }

            let catsSearchData = CatsSearchData(cats: cats,
                                                availableItemsCount: totalItemsCount,
                                                currentPage: currentPage)
            completion(.success(catsSearchData))
        }
        task.resume()
    }
    
    func fetchFullImage(withId id: String, completion: @escaping (Result<Cat, Error>) -> Void) {
        assert(!id.isEmpty, "id string could not be empty")
        
        let url = buildGetFullImageURL(id: id)
        let task = buildTask(withURL: url) { data, response, error in
            guard let cat: Cat = data?.decode() else {
                completion(.failure(ServiceError.fetchFullImage(error)))
                return
            }

            completion(.success(cat))
        }
        task.resume()
    }
    
    func buildTask(withURL url: URL, responseHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionTask {
        session.dataTask(with: buildSignedRequest(withURL: url),
                         completionHandler: responseHandler)
    }
    
    func loadImage(for url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        var storedTask: URLSessionDownloadTask?
        lock.lock()
        storedTask = downloadTasks[url.absoluteString]
        lock.unlock()
        
        guard storedTask == nil else { return }
        
        let task = session.downloadTask(with: url) { [weak self] location, _, error in
            guard let location = location,
                let data = try? Data(contentsOf: location) else {
                    completion(.failure(ServiceError.loadImage(error)))
                    return
            }
            
            self?.lock.lock()
            self?.downloadTasks.removeValue(forKey: url.absoluteString)
            self?.lock.unlock()
            
            completion(.success(data))
        }
        
        lock.lock()
        downloadTasks[url.absoluteString] = task
        lock.unlock()
        task.resume()
    }
    
    private func buildSearchImagesURL(page: Int, limit: Int) -> URL {
        let queryItems = [URLQueryItem(name: k.limit, value: String(limit)),
                          URLQueryItem(name: k.page, value: String(page)),
                          URLQueryItem(name: k.order.name, value: k.order.value),
                          URLQueryItem(name: k.mimeTypes.name, value: k.mimeTypes.value),
                          URLQueryItem(name: k.size.name, value: k.size.value)]
        return buildURL(withPath: k.searchRequestPath,
                        queryItems: queryItems)
    }
    
    private func buildGetFullImageURL(id: String) -> URL {
        buildURL(withPath: k.fullImageRequestPath + id)
    }
    
    private func buildURL(withPath path: String,
                          queryItems: [URLQueryItem]? = nil) -> URL {
        var components = URLComponents()
        components.scheme = k.scheme
        components.host = baseURL
        components.path = path
        components.queryItems = queryItems
        
        guard let url = components.url else {
            fatalError("Could not create expected URL")
        }
        return url
    }
    
    private func buildSignedRequest(withURL url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.setValue(k.apiKey.value, forHTTPHeaderField: k.apiKey.name)
        return request
    }
}
