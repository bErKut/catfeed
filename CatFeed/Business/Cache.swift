//
//  Cache.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 17.02.2021.
//  Copyright © 2021 Vlad Berkuta. All rights reserved.
//

import Foundation

final class Cache<Key: Hashable, Value: AnyObject> {
    private let nsCache: NSCache<KeyContainer, Value>
    init(totalCostLimit: Int = 200_000_000) {
        nsCache = NSCache<KeyContainer, Value>()
        nsCache.totalCostLimit = totalCostLimit
    }
    
    func insert(_ value: Value, forKey key: Key, withCost cost: Int = 0) {
        nsCache.setObject(value, forKey: KeyContainer(key), cost: cost)
    }

    func value(forKey key: Key) -> Value? {
        nsCache.object(forKey: KeyContainer(key))
    }

    func removeValue(forKey key: Key) {
        nsCache.removeObject(forKey: KeyContainer(key))
    }
}

private extension Cache {
    final class KeyContainer: NSObject {
        private let key: Key
        init(_ key: Key) { self.key = key }
        
        override var hash: Int { key.hashValue }
        
        override func isEqual(_ object: Any?) -> Bool {
            guard let value = object as? KeyContainer else {
                return false
            }
            return value.key == key
        }
    }
}
