//
//  CatItem.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 17.02.2021.
//  Copyright © 2021 Vlad Berkuta. All rights reserved.
//

import UIKit

final class CatItem: Hashable {
    var image: UIImage?
    var resolution: String {
        "\(cat.width) x \(cat.height)"
    }
    
    let cat: Cat
    init(cat: Cat) {
        self.cat = cat
    }
    
    static func == (lhs: CatItem, rhs: CatItem) -> Bool {
        lhs.cat.id == rhs.cat.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(cat.id)
    }
}
