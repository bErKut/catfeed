//
//  Store.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 04.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import Foundation
import UIKit

enum StoreError: Error {
    case general(Error)
    case imageLoad(Error)
    case imageDecode
    case urlConstruction
}

class Store {
    private enum k {
        static let workerQueueName = "com.catfeed.store.workerqueue"
        static let itemsPerPage = 50
    }
    private let service: Service
    private let workerQueue: OperationQueue
    
    private let callbackQueue: DispatchQueue
    private var page = 0
    private var pagesCount = 0
    private(set) var currentCats: [CatItem] = []
    private(set) var totalCats: Int = 0
    
    private let cache = Cache<String, UIImage>()
    
    init(callbackQueue: DispatchQueue = .main) {
        self.callbackQueue = callbackQueue
        
        workerQueue = OperationQueue()
        workerQueue.maxConcurrentOperationCount = 4
        workerQueue.name = k.workerQueueName
        service = Service(callbackQueue: workerQueue)
    }
    
    private(set) var loading = false
    
    var hasNext: Bool {
        let pageCount = Int(ceil(Double(totalCats)/Double(k.itemsPerPage)))
        return page < pageCount
    }
    
    func loadNext(completion: @escaping (Result<[CatItem], StoreError>) -> Void) {
        guard loading == false else {
            return
        }
        
        loading = true
        service.fetchCats(forPage: page, limit: k.itemsPerPage) { [weak self] result in
            guard let self = self else { return }

            self.loading = false
            
            self.callbackQueue.async {
                switch result {
                case let .success(catsData):
                    self.currentCats += catsData.cats.map { CatItem(cat: $0) }
                    self.totalCats = catsData.availableItemsCount
                    self.page = catsData.currentPage + 1
                                        
                    completion(.success(self.currentCats))
                case let .failure(error):
                    completion(.failure(StoreError.general(error)))
                }
            }
        }
    }
        
    func fetchImage(
        forItem item: CatItem,
        asThumb thumb: Bool = false,
        completion: @escaping (Result<(UIImage?, CatItem), Error>) -> Void
    ) {
        let urlString = item.cat.url
        if let image = cache.value(forKey: cacheKey(fromURL: urlString, asThumb: thumb)) {
            callbackQueue.async {
                completion(.success((image, item)))
            }
        } else if let url = URL(string: urlString) {
            service.loadImage(for: url) { [weak self] result in
                switch result {
                case let .success(data):
                    // It turned out that thecatapi ignores thumb parameter so thumbs will be created in-place
                    guard let image = data.image(asThumbnail: thumb) else {
                        completion(.failure(StoreError.imageDecode))
                        return
                    }
                    self?.cache.insert(image,
                                       forKey: cacheKey(fromURL: urlString, asThumb: thumb),
                                       withCost: thumb ? data.count : data.count / 4)
                    self?.callbackQueue.async {
                        completion(.success((image, item)))
                    }
                case let .failure(error):
                    self?.callbackQueue.async {
                        completion(.failure(StoreError.imageLoad(error)))
                    }
                    break
                }
            }
        } else {
            self.callbackQueue.async {
                completion(.failure(StoreError.urlConstruction))
            }
        }
    }
        
    func fetchFullImage(forItem item: CatItem, completion: @escaping (Result<UIImage?, Error>) -> Void) {
        service.fetchFullImage(withId: item.cat.id) { [weak self] result  in
            switch result {
            case let .success(cat):
                self?.fetchImage(forItem: CatItem(cat: cat)) { result in
                    switch result {
                    case let .success((image, _)):
                        completion(.success(image))
                        break
                    case let .failure(error):
                        completion(.failure(error))
                    }
                }
            case let .failure(error):
                completion(.failure(StoreError.general(error)))
                break
            }
        }
    }
}

fileprivate func cacheKey(fromURL url: String, asThumb thumb: Bool) -> String {
    thumb ? url.appending("_t") : url
}

