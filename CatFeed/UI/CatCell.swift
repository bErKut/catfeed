//
//  CatCell.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 05.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import UIKit

class CatCell: UICollectionViewCell {
    private enum Const {
        static let imageHeight: CGFloat = 50
        static let sidePadding: CGFloat = 16
        static let verticalPadding: CGFloat = 8
    }
    
    var image: UIImage? {
        get { imageView.image }
        set { imageView.image = newValue }
    }
    
    var size: String? {
        get { label.text }
        set { label.text = newValue }
    }
    
    private let label = UILabel()
    private let imageView = UIImageView()
    private var widthConstraint: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        for view in [label, imageView] {
            contentView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        label.textAlignment = .right
        
        widthConstraint = imageView.widthAnchor.constraint(equalToConstant: Const.imageHeight)
        
        [imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Const.sidePadding),
         imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Const.verticalPadding),
         imageView.heightAnchor.constraint(equalToConstant: Const.imageHeight),
         widthConstraint!,
         
         label.topAnchor.constraint(equalTo: imageView.topAnchor),
         label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Const.sidePadding),
         label.bottomAnchor.constraint(equalTo: imageView.bottomAnchor)
        ].activate()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        image = nil
    }
}

// MARK: Configuration
extension CatCell {
    func configure(with cat: Cat) {
        imageView.backgroundColor = .yellow
        if cat.height != 0 {
            let widthHeightRatio = CGFloat(cat.width) / CGFloat(cat.height)
            widthConstraint?.constant = Const.imageHeight * widthHeightRatio
        } else {
            widthConstraint?.constant = Const.imageHeight
        }
        size = "\(cat.width) x \(cat.height)"
    }
}
