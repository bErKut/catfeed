//
//  CatsViewController.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 03.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import UIKit

class CatsViewController: UIViewController {
    private let store: Store
    private lazy var collectionView = makeCollectionView()
    private lazy var dataSource = makeDataSource()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    init(store: Store) {
        self.store = store
        super.init(nibName: nil, bundle: nil)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        view.addSubview(collectionView)
        collectionView.pin(toParent: view)

        view.backgroundColor = .white
        loadNext()
    }
}

private let placeholderImage = UIImage(systemName: "arrow.2.circlepath")

private extension CatsViewController {
    enum Section: Int {
        case cats
    }
    
    func makeDataSource() -> UICollectionViewDiffableDataSource<Section, CatItem> {
        let cellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, CatItem> { [weak self] cell, _, item in
            cell.contentConfiguration = self?.tuneContentConfiguration(cell.defaultContentConfiguration(),
                                                                       forCatIem: item)
        }
        
        let dataSource = UICollectionViewDiffableDataSource<Section, CatItem>(collectionView: collectionView) { cv, ip, item ->UICollectionViewCell? in
            cv.dequeueConfiguredReusableCell(using: cellRegistration, for: ip, item: item)
        }

        let footerRegistration = UICollectionView.SupplementaryRegistration<FooterSupplementaryView>(
            elementKind: sectionFooterElementKind
        ) { [weak self] supplementaryView, string, indexPath in
            guard let self = self else { return }
            supplementaryView.mode = self.store.hasNext ? .loading : .noMore
            supplementaryView.backgroundColor = .lightText
        }
        
        dataSource.supplementaryViewProvider = { [weak self] view, kind, index in
            self?.collectionView.dequeueConfiguredReusableSupplementary(using: footerRegistration, for: index)
        }
        
        return dataSource
    }
    
    private func makeCollectionView() -> UICollectionView {
        let cv = UICollectionView(frame: .zero,
                                  collectionViewLayout: UICollectionViewCompositionalLayout.list)
        cv.backgroundColor = traitCollection.userInterfaceStyle == .dark ? .black : .white
        return cv
    }
        
    private func apply(items: [CatItem]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, CatItem>()
        snapshot.appendSections([.cats])
        snapshot.appendItems(items, toSection: .cats)
        dataSource.apply(snapshot)
    }
    
    private func loadNext() {
        store.loadNext { [weak self] result in
            switch result {
            case let .success(cats):
                self?.apply(items: cats)
            case let .failure(error):
                print(error)
            }
        }
    }
    
    private func tuneContentConfiguration(_ content: UIListContentConfiguration,
                                          forCatIem item: CatItem) -> UIContentConfiguration {
        var content = content
        content.image = item.image ?? placeholderImage
        content.text = item.resolution
        self.store.fetchImage(forItem: item, asThumb: true) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success((image, fetchedItem)):
                if let img = image, fetchedItem.image == nil {
                    var updatedSnapshot = self.dataSource.snapshot()
                    if let datasourceIndex = updatedSnapshot.indexOfItem(fetchedItem) {
                        let item = self.store.currentCats[datasourceIndex]
                        item.image = img
                        updatedSnapshot.reloadItems([item])
                        self.dataSource.apply(updatedSnapshot, animatingDifferences: true)
                    }
                }
            case let .failure(error):
                print(">> an error has occured")
                content.text = error.localizedDescription
                break
            }
        }
        return content
    }
}

// MARK: UICollectionViewDelegate
extension CatsViewController: UICollectionViewDelegate {
    func collectionView(_ cv: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt ip: IndexPath) {
        if ip.item == dataSource.collectionView(cv, numberOfItemsInSection: 0) - 1 {
            if store.hasNext {
                store.loadNext { [weak self] result in
                    switch result {
                    case let .success(cats):
                        self?.apply(items: cats)
                    case let .failure(error):
                        print(error)
                    }
                }
            }
        }
    }
    
    func collectionView(_ cv: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cat = dataSource.itemIdentifier(for: indexPath) {
            let detailedViewController = DetailedCatViewController(store: store, cat: cat)
            navigationController?.pushViewController(detailedViewController, animated: true)
        }
    }
}
