//
//  FooterSupplementaryView.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 17.02.2021.
//  Copyright © 2021 Vlad Berkuta. All rights reserved.
//

import UIKit

final class FooterSupplementaryView: UICollectionReusableView {
    enum Mode {
        case loading
        case noMore
    }
    
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    private let noMoreLabel = UILabel()
    private let contentView = UIView()
    
    var mode: Mode = .loading {
        didSet {
            noMoreLabel.isHidden = mode == .loading
            activityIndicator.isHidden = mode == .noMore
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addContentView()
        addActivityIndicator()
        addNoMoreLabel()
        customizeAppearance()
    }
    
    private func addContentView() {
        addSubview(contentView)
        contentView.pin(toParent: self)
    }
    
    private func addActivityIndicator() {
        contentView.addSubview(activityIndicator)
        activityIndicator.center(inParent: contentView)
        activityIndicator.startAnimating()
    }
    
    private func addNoMoreLabel() {
        contentView.addSubview(noMoreLabel)
        noMoreLabel.pin(toParent: self)
        
        noMoreLabel.text = "No more cats left 🙀"
        noMoreLabel.font = UIFont.boldSystemFont(ofSize: 30)
        noMoreLabel.textAlignment = .center
    }
    
    private func customizeAppearance() {
        switch traitCollection.userInterfaceStyle {
        case .dark:
            contentView.backgroundColor = .black
            noMoreLabel.textColor = .systemYellow
        case .light:
            contentView.backgroundColor = .white
            noMoreLabel.textColor = .magenta
        default: break
        }
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }
}
