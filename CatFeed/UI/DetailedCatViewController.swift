//
//  DetailedCatViewController.swift
//  CatFeed
//
//  Created by Vlad Berkuta on 13.10.2020.
//  Copyright © 2020 Vlad Berkuta. All rights reserved.
//

import UIKit

class DetailedCatViewController: UIViewController {
    private let store: Store
    private let imageView = UIImageView()
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    private let textView = UITextView()
    private let catItem: CatItem

    init(store: Store, cat: CatItem) {
        self.store = store
        self.catItem = cat
        super.init(nibName: nil, bundle: nil)
        textView.isEditable = false
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = traitCollection.userInterfaceStyle == .dark ? .black : .white
        activityIndicator.startAnimating()
        store.fetchFullImage(forItem: catItem) { [weak self] result in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                switch result {
                case let .success(image):
                    self?.imageView.image = image
                case let .failure(error):
                    print("failed to load full image: ", error.localizedDescription)
                    break
                }
            }
        }
        
        imageView.contentMode = .scaleAspectFit
        textView.font = Const.font
        textView.text = prettyPrinted()
        view.addSubview(imageView)
        imageView.addSubview(activityIndicator)
        view.addSubview(textView)
        installConstraints()
    }
}

private extension DetailedCatViewController {
    enum Const {
        static let padding: CGFloat = 16
        static let font = UIFont.systemFont(ofSize: 16)
    }
    
    func installConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndicator.center(inParent: imageView)
        
        [imageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Const.padding),
         imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Const.padding),
         imageView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Const.padding),
         imageView.heightAnchor.constraint(equalTo: view.widthAnchor, constant: -2 * Const.padding),
                  
         textView.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
         textView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Const.padding),
         textView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
         textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Const.padding)
        ].activate()
    }
    
    func prettyPrinted() -> String? {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        var string: String?
        do {
            let jsonData = try encoder.encode(catItem.cat)
            string = String(data: jsonData, encoding: .utf8)
        }
        catch {
            print(error.localizedDescription)
        }
        return string
    }
}
